// Test I2S ADC project.

#include <string.h>

#include "esp_err.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "driver/adc.h"
#include "driver/i2s.h"

static const char *TAG = "i2s-test";

#define SAMPLE_RATE (24000U)

#define NUM_SAMPLES (64U)
#define BUFFER_SIZE_BYTES (NUM_SAMPLES * 2 * sizeof (uint16_t))

// Shows the first 16 bytes of data.
static void show_some_data(uint16_t * data)
{
    const unsigned rows = 8;
    for (unsigned row = 0; row < rows; row++)
    {
        ESP_LOGI(
            TAG,
            "CH: %d %d %d %d %d %d %d %d",
            (data[8*row + 0] & 0xF000) >> 12,
            (data[8*row + 1] & 0xF000) >> 12,
            (data[8*row + 2] & 0xF000) >> 12,
            (data[8*row + 3] & 0xF000) >> 12,
            (data[8*row + 4] & 0xF000) >> 12,
            (data[8*row + 5] & 0xF000) >> 12,
            (data[8*row + 6] & 0xF000) >> 12,
            (data[8*row + 7] & 0xF000) >> 12
        );
    }
}

static void i2s_read_task(QueueHandle_t * i2s_queue, uint16_t * data)
{
    size_t data_remaining;

    char * head;

    i2s_event_t evt;

    int idx;

    for (idx = 0; idx < 3; idx++)
    {
        if (xQueueReceive(*i2s_queue, &evt, portMAX_DELAY) == pdPASS)
        {
            if (evt.type == I2S_EVENT_RX_DONE)
            {
                data_remaining = BUFFER_SIZE_BYTES;
                head = (char *) data;

                while (data_remaining > 0)
                {
                    size_t data_recd = 0;

                    ESP_ERROR_CHECK(
                        i2s_read(
                            I2S_NUM_0,
                            head, data_remaining,
                            &data_recd,
                            portMAX_DELAY)
                    );

                    head += data_recd;
                    data_remaining -= data_recd;
                }

                show_some_data(data);
            }
        }
    }
}

// Stops the I2S reading task.
static void i2s_stop_reading(void)
{
    ESP_LOGI(TAG, "disabling I2S ADC");
    ESP_ERROR_CHECK(i2s_adc_disable(I2S_NUM_0));
}

// Starts the I2S reading task.
static void i2s_start_reading(void)
{
    ESP_LOGI(TAG, "enabling I2S ADC");
    ESP_ERROR_CHECK(i2s_zero_dma_buffer(I2S_NUM_0));
    ESP_ERROR_CHECK(i2s_adc_enable(I2S_NUM_0));
}

static void app_i2s_adc_init(QueueHandle_t * i2s_queue)
{
    int i2s_num = I2S_NUM_0;

    i2s_config_t i2s_config = {
        .mode = I2S_MODE_MASTER | I2S_MODE_RX | I2S_MODE_ADC_BUILT_IN,
        .sample_rate =  SAMPLE_RATE,
        .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,
        .communication_format = I2S_COMM_FORMAT_I2S,
        .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
        .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,
        .dma_buf_count = 2,
        .dma_buf_len = NUM_SAMPLES,
        .use_apll = 0,
        .fixed_mclk = 0
    };

    i2s_pin_config_t pin_config = {
        .bck_io_num = -1,
        .ws_io_num = GPIO_NUM_25,
        .data_out_num = -1,
        .data_in_num = -1                                               //Not used
    };

    i2s_driver_install(i2s_num, &i2s_config, 4, i2s_queue);
    i2s_set_clk(i2s_num, SAMPLE_RATE, I2S_BITS_PER_SAMPLE_16BIT, I2S_CHANNEL_STEREO);
    i2s_set_pin(i2s_num, &pin_config);
    i2s_set_adc_mode(ADC_UNIT_1, ADC1_CHANNEL_6);

    // adc_set_i2s_data_pattern(ADC_UNIT_1, 0, ADC_CHANNEL_6, ADC_WIDTH_BIT_12, ADC_ATTEN_DB_11);
    adc_set_i2s_data_pattern(ADC_UNIT_1, 1, ADC_CHANNEL_5, ADC_WIDTH_BIT_12, ADC_ATTEN_DB_11);
    adc_set_i2s_data_len(ADC_UNIT_1, 2);
}

// Entry point for application (called by ESP IDF's own main() function.)
void app_main()
{
    const unsigned delay = 1000 / portTICK_PERIOD_MS;

    QueueHandle_t i2s_queue;

    // Initialise I2S ADC.
    app_i2s_adc_init(&i2s_queue);

    uint16_t * data = malloc(BUFFER_SIZE_BYTES);

    memset(data, 0, BUFFER_SIZE_BYTES);

    while (1)
    {
        i2s_start_reading();
        i2s_read_task(&i2s_queue, data);
        i2s_stop_reading();
        vTaskDelay(delay);
    }

    free(data);
}
